/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef SERVERTCP_H
#define SERVERTCP_H

#include <QtNetwork/QTcpServer>
#include <QList>

class ServerTCPConnection;
class ProcessMPlayer;

/**
 * @brief
 *
 */
class ServerTCP : public QTcpServer
{
  friend class ServerTCPConnection;

  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param mplayer
   * @param parent
   */
  explicit ServerTCP(ProcessMPlayer* mplayer, QObject *parent = 0);


  /**
   * @brief Force media hash checking
   *
   * @param f
   */
  void setForceHashCheck(bool f = true);

signals:
  /**
   * @brief
   *
   * @param msg
   */
  void message(const QString& msg);

  /**
   * @brief
   *
   * @param nick
   * @param message
   */
  void chatMessage(const QString& nick, const QString& message);

protected:
  /**
   * @brief
   *
   * @param handle
   */
  void incomingConnection(int handle);

public slots:
  /**
   * @brief
   *
   */
  void synchronizeClients(int second);

  /**
   * @brief
   *
   */
  void pauseClients();

  /**
   * @brief
   *
   */
  void stopClients();

  /**
   * @brief
   *
   */
  void checkMedia();

  /**
   * @brief
   *
   * @param nick
   * @param message
   */
  void sendChatMessage(const QString& nick, const QString& message);

  /**
   * @brief Used for periodic tasks like media checksum check
   *
   * @param
   */
  void timerEvent(QTimerEvent *);

private slots:
  /**
   * @brief
   *
   */
  void clientDisconnected();

private:
  QList<ServerTCPConnection*> myConnections; /**< The connected clients */
  ProcessMPlayer* myMPlayer; /**< The media player */
  bool myForceHashCheckFlag; /**< Are we checking the remote media checksum? */

  /**
   * @brief
   *
   * @return bool
   */
  bool forceHashCheck() const;
};

#endif // SERVERTCP_H
