/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "ClientTCP.h"
#include "ProcessMPlayer.h"
#include "Common.h"
#include "Protocol.h"
#include <QMessageBox>

ClientTCP::ClientTCP(ProcessMPlayer *mplayer, QObject *parent) :
  QTcpSocket(parent),
  myMPlayer(mplayer)
{
  myStream.setDevice(this);
  connect(this, SIGNAL(readyRead()), SLOT(readAndParse()));
  connect(this, SIGNAL(connected()), SLOT(connectedToServer()));
  connect(this, SIGNAL(disconnected()), SLOT(disconnectedFromServer()));
}

void ClientTCP::readAndParse()
{
  quint8 c;
  while(!myStream.atEnd())
  {
    myStream >> c;
    switch(c)
    {
    case SvcSeek:
      int t;
      myStream >> t;
      if(!myMPlayer->isPlaying())
        myMPlayer->startPlaying();
      myMPlayer->seek(t);

      emit message(tr("Synchronizing..."));
      break;
    case SvcPause:
      if(!myMPlayer->isPaused())
        myMPlayer->pause();

      emit message(tr("Pausing..."));
      break;
    case SvcStop:
      myMPlayer->stop();

      emit message(tr("Stopping..."));
      break;
    case SvcHash:
      checkMedia();
      break;
    case SvcChat:
    {
      QString nick;
      QString message;
      myStream >> nick >> message;

      emit chatMessage(nick, message);
      break;
    }
    case SvcMessage:
    {
      QString msg;
      myStream >> msg;
      emit message(msg);
      break;
    }
    case SvcDrop:
    {
      QString reason;
      myStream >> reason;
      QMessageBox::information(NULL, tr("Connection Dropped"), reason);
      disconnectFromHost();
      break;
    }
    }
  }
}

void ClientTCP::pause()
{
  myStream << (quint8)ClcPause;
}

void ClientTCP::stop()
{
  myStream << (quint8)ClcStop;
}

void ClientTCP::sendChatMessage(const QString &nick, const QString &message)
{
  myStream << (quint8)ClcChat << nick << message;
}

void ClientTCP::synchronize(int second)
{
  myStream << (quint8)ClcSeek << second;
}

void ClientTCP::checkMedia()
{
  myStream << (quint8)ClcHash << myMPlayer->currentFileHash();
}

void ClientTCP::disconnectedFromServer()
{
  emit message(tr("Disconnected."));
}

void ClientTCP::connectedToServer()
{
  emit message(tr("Connected."));
}

void ClientTCP::sendMessage(const QString &message)
{
  myStream << (quint8)ClcMessage << message;
}
