/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef DIALOGSERVERSETUP_H
#define DIALOGSERVERSETUP_H

#include <QDialog>

namespace Ui {
  class DialogServerSetup;
}

/**
 * @brief
 *
 */
class DialogServerSetup : public QDialog
{
  Q_OBJECT
  
public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit DialogServerSetup(QWidget *parent = 0);

  /**
   * @brief
   *
   */
  ~DialogServerSetup();

  /**
   * @brief
   *
   * @return quint16
   */
  quint16 port() const;

  /**
   * @brief
   *
   * @return bool
   */
  bool checkMediaHash() const;

private:
  Ui::DialogServerSetup *ui; /**< TODO */
};

#endif // DIALOGSERVERSETUP_H
