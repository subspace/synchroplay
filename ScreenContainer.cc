/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "ScreenContainer.h"
#include <QVBoxLayout>
#include <QResizeEvent>
#include <QDebug>

ScreenContainer::ScreenContainer(QWidget *parent) :
  Screen(parent),
  myScreen(new Screen(this)),
  myAspectRatio(16/9.0f),
  myHideCursorTimerId(0)
{
  // Set black background
  setAutoFillBackground(true);
  QPalette p = palette();
  p.setColor(backgroundRole(), QColor(0, 0, 0));
  setPalette(p);

  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  setFocusPolicy(Qt::StrongFocus);

  installEventFilter(this);
}

WId ScreenContainer::windowId() const
{
  return myScreen->winId();
}

void ScreenContainer::resizeEvent(QResizeEvent *)
{
  myOffsetX = myOffsetY = 0;
  updateScreen();
}

void ScreenContainer::setAspectRatio(double ratio)
{
  myAspectRatio = ratio;
  updateScreen();
//  qDebug() << myAspectRatio;
}

void ScreenContainer::updateScreen()
{
  int windowWidth = size().width();
  int windowHeight = size().height();
  int w = windowWidth;
  int h = windowHeight;
  int x = 0;
  int y = 0;

  if(myAspectRatio != 0)
  {
    int pos1Width = windowWidth;
    int pos1Height = windowWidth / myAspectRatio + 0.5;

    int pos2Height = windowHeight;
    int pos2Width = windowHeight * myAspectRatio + 0.5;
    if(pos1Height <= windowHeight)
    {
      w = pos1Width;
      h = pos1Height;
      y = (windowHeight - h) / 2;
    }
    else
    {
      w = pos2Width;
      h = pos2Height;
      x = (windowWidth - w) / 2;
    }
  }

  myScreen->move(x, y);
  myScreen->resize(w, h);

  myOriginalX = x;
  myOriginalY = y;
  myOriginalWidth = w;
  myOriginalHeight = h;
}

void ScreenContainer::mouseDoubleClickEvent(QMouseEvent *e)
{
  if(e->button() == Qt::LeftButton)
  {
    e->accept();
    emit doubleClicked();
  }
  else
    e->ignore();
}

bool ScreenContainer::eventFilter(QObject *, QEvent *e)
{
  if(e->type() == QEvent::MouseMove)
  {
    if(myHideCursorTimerId)
      killTimer(myHideCursorTimerId);

    setCursor(Qt::ArrowCursor);
    myHideCursorTimerId = startTimer(3000);
  }
  return false; // Don't eat any event
}

void ScreenContainer::timerEvent(QTimerEvent *e)
{
  if(e->timerId() == myHideCursorTimerId)
  {
    killTimer(myHideCursorTimerId);
    myHideCursorTimerId = 0;
    setCursor(Qt::BlankCursor);
  }
}
