/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef DIALOGCHAT_H
#define DIALOGCHAT_H

#include <QDialog>

class QStringListModel;

namespace Ui {
  class DialogChat;
}

/**
 * @brief Simple chatting window
 *
 */
class DialogChat : public QDialog
{
  Q_OBJECT

public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit DialogChat(QWidget *parent = 0);
  /**
   * @brief
   *
   */
  ~DialogChat();

signals:
  /**
   * @brief
   *
   * @param nick
   * @param message
   */
  void message(const QString& nick, const QString& message);

public slots:
  /**
   * @brief
   *
   * @param nick
   * @param message
   */
  void addMessage(const QString& nick, const QString& message);

private:
  Ui::DialogChat *ui; /**< TODO */
  QStringListModel* myModel; /**< TODO */

private slots:
  /**
   * @brief
   *
   */
  void sendMessage();
};

#endif // DIALOGCHAT_H
