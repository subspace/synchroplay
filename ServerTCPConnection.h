/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef SERVERTCPCONNECTION_H
#define SERVERTCPCONNECTION_H

#include <QtNetwork/QTcpSocket>
#include <QDataStream>

class ServerTCP;
class ProcessMPlayer;
/**
 * @brief
 *
 */
class ServerTCPConnection : public QTcpSocket
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param mplayer
   * @param parent
   */
  explicit ServerTCPConnection(ProcessMPlayer* mplayer, ServerTCP *parent = 0);

  /**
   * @brief Synchronizes the client with the server, since the seek function only seeks
   * to keyframes we have to seek ourselves to the keyframe before sending the seek command to
   * the client.
   *
   */
  void synchronize(int second);
  /**
   * @brief
   *
   */
  void pause();
  /**
   * @brief
   *
   */
  void stop();

  /**
   * @brief Checks if the media on the client is the same on the server
   *
   */
  void checkMedia();

  /**
   * @brief
   *
   * @param nick
   * @param message
   */
  void sendChatMessage(const QString& nick, const QString& message);

  /**
   * @brief Sends a message to this client
   *
   * @param message
   */
  void sendMessage(const QString& message);

private slots:
  /**
   * @brief
   *
   */
  void readAndParse();

private:
  ServerTCP*      myServer;
  ProcessMPlayer* myMPlayer; /**< TODO */
  QDataStream     myStream;
};

#endif // SERVERTCPCONNECTION_H
