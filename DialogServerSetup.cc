/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "DialogServerSetup.h"
#include "ui_DialogServerSetup.h"

DialogServerSetup::DialogServerSetup(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::DialogServerSetup)
{
  ui->setupUi(this);
  ui->editPort->setValidator(new QIntValidator(1, 65535, this));
  ui->editPort->setText("22777");
  ui->checkHash->setChecked(true);
}

DialogServerSetup::~DialogServerSetup()
{
  delete ui;
}

quint16 DialogServerSetup::port() const
{
  return ui->editPort->text().toUShort();
}

bool DialogServerSetup::checkMediaHash() const
{
  return ui->checkHash->isChecked();
}
