#-------------------------------------------------
#
# Project created by QtCreator 2012-12-11T23:30:25
#
#-------------------------------------------------

QT       += core gui network widgets

TARGET = synchroplay
TEMPLATE = app


SOURCES += main.cc\
        WindowMain.cc \
    ProcessMPlayer.cc \
    DialogAbout.cc \
    Screen.cc \
    ScreenContainer.cc \
    LabelTime.cc \
    SliderPlayback.cc \
    SliderVolume.cc \
    SynchroPlay.cc \
    ServerTCP.cc \
    ServerTCPConnection.cc \
    ClientTCP.cc \
    DialogClientJoin.cc \
    DialogServerSetup.cc \
    Common.cc \
    LabelStatus.cc \
    DialogChat.cc

HEADERS  += WindowMain.h \
    ProcessMPlayer.h \
    DialogAbout.h \
    Screen.h \
    ScreenContainer.h \
    LabelTime.h \
    SliderPlayback.h \
    SliderVolume.h \
    SynchroPlay.h \
    ServerTCP.h \
    ServerTCPConnection.h \
    ClientTCP.h \
    DialogClientJoin.h \
    DialogServerSetup.h \
    Common.h \
    LabelStatus.h \
    Protocol.h \
    DialogChat.h

FORMS    += WindowMain.ui \
    DialogAbout.ui \
    DialogClientJoin.ui \
    DialogServerSetup.ui \
    DialogChat.ui

