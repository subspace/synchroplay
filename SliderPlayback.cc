/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "SliderPlayback.h"
#include "ProcessMPlayer.h"
#include <QMouseEvent>
#include <QDebug>

SliderPlayback::SliderPlayback(ProcessMPlayer* mplayer, QWidget *parent) :
  QSlider(Qt::Horizontal, parent),
  myMPlayer(mplayer)
{
  setLength(0);
  setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
  setPageStep(60);
  setSingleStep(60);
  connect(myMPlayer, SIGNAL(playbackSecond(int)), SLOT(setValue(int)));
  connect(myMPlayer, SIGNAL(playbackLength(int)), SLOT(setLength(int)));
}

void SliderPlayback::setLength(int length)
{
  setMinimum(0);
  setMaximum(length);
}

void SliderPlayback::mousePressEvent(QMouseEvent *ev)
{
  QSlider::mousePressEvent(ev);
  qDebug() << "pre";
  disconnect(myMPlayer, SIGNAL(playbackSecond(int)), this, SLOT(setValue(int)));
}

void SliderPlayback::mouseReleaseEvent(QMouseEvent *ev)
{
  QSlider::mouseReleaseEvent(ev);
  qDebug() << "rel";
  myMPlayer->seek(value());
  connect(myMPlayer, SIGNAL(playbackSecond(int)), SLOT(setValue(int)));
  emit resync();
}

