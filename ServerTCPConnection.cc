/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "ServerTCPConnection.h"
#include "ProcessMPlayer.h"
#include "Common.h"
#include "ServerTCP.h"
#include "Protocol.h"

ServerTCPConnection::ServerTCPConnection(ProcessMPlayer* mplayer, ServerTCP *parent) :
  QTcpSocket(parent),
  myServer(parent),
  myMPlayer(mplayer)
{
  myStream.setDevice(this);
  connect(this, SIGNAL(readyRead()), SLOT(readAndParse()));
  connect(this, SIGNAL(disconnected()), SLOT(deleteLater()));
}

void ServerTCPConnection::readAndParse()
{
  quint8 c;
  while(!myStream.atEnd())
  {
    myStream >> c;
    switch(c)
    {
    case ClcHash:
    {
      QByteArray hash;
      myStream >> hash;
      if(hash != myMPlayer->currentFileHash() && myServer->forceHashCheck())
      {
        myStream << (quint8)SvcDrop << QString(tr("Bad media hash."));
        disconnectFromHost();
      }
      break;
    }
    case ClcChat:
    {
      QString nick, message;
      myStream >> nick >> message;
      myServer->sendChatMessage(nick, message);
      break;
    }
    case ClcMessage:
    {
      QString msg;
      myStream >> msg;
      myServer->message(msg);
      break;
    }
    case ClcDrop:
      disconnectFromHost();
      break;
    case ClcSeek: // A client requested a Seek command, the server will try to sync to the client keyframe and request the other clients to do the same
    {
      int sec;
      myStream >> sec;
      myServer->synchronizeClients(sec);
      break;
    }
    case ClcPause:
    {
      myMPlayer->pause();
      myServer->pauseClients();
      break;
    }
    case ClcStop:
    {
      myMPlayer->stop();
      myServer->stopClients();
      break;
    }
    }
  }
}

void ServerTCPConnection::synchronize(int second)
{
  myStream << (quint8)SvcSeek << second;
}

void ServerTCPConnection::pause()
{
  myStream << (quint8)SvcPause; /* << myMPlayer->currentPlaybackSecond();*/
}

void ServerTCPConnection::stop()
{
  myStream << (quint8)SvcStop;
}

void ServerTCPConnection::checkMedia()
{
  myStream << (quint8)SvcHash;
}

void ServerTCPConnection::sendChatMessage(const QString &nick, const QString &message)
{
  myStream << (quint8)SvcChat << nick << message;
}

void ServerTCPConnection::sendMessage(const QString &message)
{
  myStream << (quint8)SvcMessage << message;
}
