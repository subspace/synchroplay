/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef PROCESSMPLAYER_H
#define PROCESSMPLAYER_H

#include <QProcess>
#include <QWidget>

/**
 * @brief Controls mplayer process
 *
 */
class ProcessMPlayer : public QProcess
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit ProcessMPlayer(QObject *parent = 0);
  /**
   * @brief Sets the window ID (HWND on windows) to where mplayer should output video
   *
   * @param windowID
   */
  void setWindowID(WId windowID);
  /**
   * @brief Returns current playback position
   *
   * @return int The current second of playback
   */
  int currentPlaybackSecond() const;

  /**
   * @brief Returns the currently playing media file path
   *
   * @return const QString The path to the file
   */
  const QString& currentlyPlayingMedia() const;

  /**
   * @brief Gets the currently playing file hash, generates it if needed
   *
   * @return const QByteArray
   */
  const QByteArray& currentFileHash();

  bool isPlaying() const;
  bool isStopped() const;
  bool isPaused() const;

signals:
  /**
   * @brief Signals if we are currently playing something or not
   *
   * @param isPlaying
   */
  void playing(bool isPlaying);
  /**
   * @brief Signals the resolution of the media
   *
   * @param width
   * @param height
   */
  void windowResolution(int width, int height);
  /**
   * @brief Signals the detected aspect ratio of the media
   *
   * @param ratio
   */
  void aspectRatio(double ratio);
  /**
   * @brief Signals the time we are at on the media playback
   *
   * @param second
   */
  void playbackSecond(int second);
  /**
   * @brief Signals the duration of the media
   *
   * @param length
   */
  void playbackLength(int length);
  /**
   * @brief Signals when mplayer finished loading and started playback
   *
   */
  void loaded();

  /**
   * @brief
   *
   * @param sec
   */
  void playback(int sec);

  /**
   * @brief
   *
   * @param msg
   */
  void message(const QString& msg);

public slots:
  /**
   * @brief Starts playing a media file
   *
   * @param fileName
   * @param secs
   * @return bool True on success false otherwise
   */
  bool startPlaying(const QString& fileName = "", int secs = 0);
  /**
   * @brief Stops mplayer
   *
   */
  void stop();
  /**
   * @brief Pauses the media
   *
   */
  void pause();
  /**
   * @brief Seeks to a given position
   *
   * @param secs Seconds to seek to
   * @param mode On mode 0 it will skip the given secs, mode 2 skips to the given secs
   */
  void seek(int secs, int mode = 2);
  /**
   * @brief Sets the volume on mplayer
   *
   * @param volume 0 - 100 range
   */
  void setVolume(int volume);

  void setFullScreen(bool fs = true);
protected slots:
  /**
   * @brief Sends a command to mplayer
   *
   * @param command
   */
  void sendCommand(const QString& command);
  /**
   * @brief Reads and parses mplayer output
   *
   */
  void readAndParse();

private:
  /**
   * @brief Current state of mplayer
   *
   */
  enum State { Playing, Paused, Stopped };

  WId myWindowID; /**< Video output window id */
  State myState; /**< State */
  QString myCurrentFile; /**< Current media being played */
  int myLastPlaybackSecond; /**< Used to signal the second when it changes only */
  bool myLoadedFlag; /**< Used to know mplayer just loaded */
  bool myFullScreenFlag; /**< Indicates whether we are in fullscreen mode */
  QByteArray myCurrentFileHash;
  bool myPausingFlag;
};

#endif // PROCESSMPLAYER_H
