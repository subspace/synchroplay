/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef SLIDERPLAYBACK_H
#define SLIDERPLAYBACK_H

#include <QSlider>

class ProcessMPlayer;

/**
 * @brief Slider that controls mplayer position on media
 *
 */
class SliderPlayback : public QSlider
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param mplayer
   * @param parent
   */
  explicit SliderPlayback(ProcessMPlayer* mplayer, QWidget *parent = 0);

signals:
  void resync();

public slots:
  /**
   * @brief
   *
   * @param length
   */
  void setLength(int length);

protected:

  void mousePressEvent(QMouseEvent *ev);
  void mouseReleaseEvent(QMouseEvent *ev);

private:
  ProcessMPlayer* myMPlayer; /**< The mplayer process */
};

#endif // SLIDERPLAYBACK_H
