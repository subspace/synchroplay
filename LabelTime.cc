/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "LabelTime.h"
#include "ProcessMPlayer.h"

LabelTime::LabelTime(QWidget *parent) :
  QLabel(parent),
  myCurrentTime(0),
  myDuration(0)
{
  setText("00:00:00 / 00:00:00");
}

void LabelTime::setTime(int secs)
{
  myCurrentTime = secs;
  updateTime();
}

void LabelTime::setDuration(int secs)
{
  myDuration = secs;
  updateTime();
}

static const QString formatTime(int secs)
{
  int t = secs;
  int hours = (int)t / 3600;
  t -= hours * 3600;
  int minutes = (int)t / 60;
  t -= minutes * 60;
  int seconds = t;

  return QString("%1:%2:%3").arg(hours, 2, 10, QChar('0')).arg(minutes, 2, 10, QChar('0')).arg(seconds, 2, 10, QChar('0'));
}

void LabelTime::updateTime()
{
  setText(formatTime(myCurrentTime) + " / " + formatTime(myDuration));
}


