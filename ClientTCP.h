/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef CLIENTTCP_H
#define CLIENTTCP_H

#include <QtNetwork/QTcpSocket>
#include <QDataStream>

class ProcessMPlayer;

/**
 * @brief
 *
 */
class ClientTCP : public QTcpSocket
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param mplayer
   * @param parent
   */
  explicit ClientTCP(ProcessMPlayer* mplayer, QObject *parent = 0);

  /**
   * @brief
   *
   */
  void synchronize(int second);

  /**
   * @brief Checks if the media we have is the same on the server side
   *
   */
  void checkMedia();

  void pause();
  void stop();

signals:
  /**
   * @brief
   *
   * @param msg
   */
  void message(const QString& msg);

  /**
   * @brief
   *
   * @param nick
   * @param message
   */
  void chatMessage(const QString& nick, const QString& message);

public slots:
  /**
   * @brief
   *
   * @param nick
   * @param message
   */
  void sendChatMessage(const QString& nick, const QString &message);

  /**
   * @brief Sends a message to the server
   *
   * @param message
   */
  void sendMessage(const QString& message);

private slots:
  /**
   * @brief
   *
   */
  void readAndParse();

  /**
   * @brief
   *
   */
  void connectedToServer();
  /**
   * @brief
   *
   */
  void disconnectedFromServer();

private:
  ProcessMPlayer* myMPlayer; /**< TODO */
  QDataStream myStream;
};

#endif // CLIENTTCP_H
