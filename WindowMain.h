/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef WINDOWMAIN_H
#define WINDOWMAIN_H

#include <QMainWindow>

class ProcessMPlayer;
class ScreenContainer;
class ServerTCP;
class ClientTCP;
class QLabel;
class DialogChat;

namespace Ui {
  class WindowMain;
}

/**
 * @brief Main Window
 *
 */
class WindowMain : public QMainWindow
{
  Q_OBJECT

public:
  /**
   * @brief Constructor
   *
   * @param parent
   */
  explicit WindowMain(QWidget *parent = 0);

  /**
   * @brief
   *
   */
  ~WindowMain();

private slots:
  /**
   * @brief Opens a media file
   *
   */
  void on_actionOpen_triggered();

  /**
   * @brief Shows about dialog
   *
   */
  void on_actionAbout_triggered();

  /**
   * @brief As a client sync to the host time, as a server make all the clients sync to our time
   *
   */
  void on_actionSync_triggered();

  /**
   * @brief Hosts a synchronized play
   *
   */
  void on_actionHost_triggered();

  /**
   * @brief Joins a synchronized play
   *
   */
  void on_actionJoin_triggered();

  /**
   * @brief
   *
   */
  void on_actionStopSynchronizing_triggered();

  /**
   * @brief
   *
   */
  void on_actionPlay_triggered();

  /**
   * @brief
   *
   */
  void on_actionPause_triggered();

  /**
   * @brief
   *
   */
  void on_actionStop_triggered();

  /**
   * @brief
   *
   */
  void toggleFullScreen();

  /**
   * @brief
   *
   * @param msg
   */
  void showStatusBarMessage(const QString& msg);

  /**
   * @brief
   *
   */
  void on_actionChat_triggered();

  /**
   * @brief Opens the chat window and add a message
   *
   * @param nick
   * @param msg
   */
  void openChatDialog(const QString& nick, const QString& msg);

  /**
   * @brief
   *
   */
  void closeChatDialog();

  void delayedSync();

protected:
  /**
   * @brief
   *
   * @param e
   */
  void closeEvent(QCloseEvent *e);

  /**
   * @brief Used for showing the menu bar and controls tool bar
   *
   * @param e
   */
  void mouseMoveEvent(QMouseEvent *e);

private:
  /**
   * @brief
   *
   */
  enum NetworkType { None, Host, Client };

  Ui::WindowMain*   ui; /**< The UI */
  ProcessMPlayer*   myMPlayer; /**< Controls MPlayer */
  ScreenContainer*  myScreen; /**< MPlayer video output screen */
  QString           myCurrentFile; /**< Current media being played */
  ServerTCP*        myServer; /**< Our server in case we host a synchronized play */
  ClientTCP*        myClient; /**< Our client in case we are joining a synchronized play */
  NetworkType       myNetworkType;  /**< Indicates whether this app is acting as host, client or none */
  bool              myFullScreenFlag; /**< Indicates whether we are in fullscreen mode or not */
  DialogChat*       myChatDialog;  /**< Chat dialog */

  void reconnectChatSlots();
};

#endif // WINDOWMAIN_H
