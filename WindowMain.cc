/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "WindowMain.h"
#include "ui_WindowMain.h"
#include "ProcessMPlayer.h"
#include "DialogAbout.h"
#include "ScreenContainer.h"
#include "SliderPlayback.h"
#include "SliderVolume.h"
#include "LabelTime.h"
#include "ServerTCP.h"
#include "ClientTCP.h"
#include "DialogClientJoin.h"
#include "DialogServerSetup.h"
#include "DialogChat.h"
#include <QFileDialog>
#include <QCloseEvent>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QDebug>

// TODO: add aspect ratio menu
// TODO: resize the window to the received resolution of the movie on starting playback
// TODO: drag and drop subs
// TODO: pop the chat window up on incoming messages

WindowMain::WindowMain(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::WindowMain),
  myMPlayer(new ProcessMPlayer()),
  myServer(NULL),
  myClient(NULL),
  myNetworkType(None),
  myFullScreenFlag(false),
  myChatDialog(0)
{
  myMPlayer->setObjectName("mplayer");
  ui->setupUi(this);

  setMouseTracking(true);

  SliderPlayback* playbackSlider = new SliderPlayback(myMPlayer, this);
  SliderVolume* volumeSlider = new SliderVolume(this);
  LabelTime* lt = new LabelTime(this);

  myScreen = ui->centralWidget;

  ui->actionPlay->setEnabled(false);
  ui->mainToolBar->addWidget(playbackSlider);
  ui->mainToolBar->addWidget(volumeSlider);
  ui->mainToolBar->setFloatable(false);
  ui->mainToolBar->setMovable(false);
  ui->statusBar->addPermanentWidget(lt);
  ui->actionStop->setEnabled(false);

  connect(ui->actionExit, &QAction::triggered, this, &WindowMain::close);
  connect(playbackSlider, &SliderPlayback::valueChanged, lt, &LabelTime::setTime);
  connect(playbackSlider, &SliderPlayback::resync, this, &WindowMain::delayedSync);
  connect(volumeSlider, &SliderVolume::valueChanged, myMPlayer, &ProcessMPlayer::setVolume);
  connect(myMPlayer, &ProcessMPlayer::loaded, volumeSlider, &SliderVolume::resetVolume);
  connect(myMPlayer, &ProcessMPlayer::playing, ui->actionPlay, &QAction::setDisabled);
  connect(myMPlayer, &ProcessMPlayer::playing, ui->actionPause, &QAction::setEnabled);
  connect(myMPlayer, &ProcessMPlayer::aspectRatio, myScreen, &ScreenContainer::setAspectRatio);
  connect(myMPlayer, &ProcessMPlayer::playbackLength, lt, &LabelTime::setDuration);
  connect(myScreen, &ScreenContainer::doubleClicked, this, &WindowMain::toggleFullScreen);
}

WindowMain::~WindowMain()
{
  delete myMPlayer;
  delete ui;
}

void WindowMain::on_actionAbout_triggered()
{
  DialogAbout dlg;
  dlg.exec();
}

void WindowMain::delayedSync()
{
  connect(myMPlayer, SIGNAL(playbackSecond(int)), SLOT(on_actionSync_triggered()));
}

void WindowMain::closeEvent(QCloseEvent *e)
{
  myMPlayer->stop();
  e->accept();
}

void WindowMain::on_actionOpen_triggered()
{
  QString fileName = QFileDialog::getOpenFileName(this, tr("Select any media file to open..."));
  if(fileName.isEmpty())
    return;
  myMPlayer->setWindowID(myScreen->windowId());
  myMPlayer->startPlaying(fileName);
  myCurrentFile = fileName;
  ui->actionStop->setEnabled(true);
  setWindowTitle(fileName.section("/", -1) + " - SynchroPlay");
}

void WindowMain::on_actionSync_triggered()
{
  if(myNetworkType == Client)
    myClient->synchronize(myMPlayer->currentPlaybackSecond());
  else if(myNetworkType == Host)
    myServer->synchronizeClients(myMPlayer->currentPlaybackSecond());
  disconnect(myMPlayer, SIGNAL(playbackSecond(int)), this, SLOT(on_actionSync_triggered()));
}

void WindowMain::on_actionHost_triggered()
{
  on_actionStopSynchronizing_triggered();

  DialogServerSetup dlg(this);
  int ret = dlg.exec();
  if(ret == QDialog::Rejected)
    return;

  myServer = new ServerTCP(myMPlayer, this);
  connect(myServer, SIGNAL(message(QString)), SLOT(showStatusBarMessage(QString)));
  connect(myServer, SIGNAL(chatMessage(QString,QString)), SLOT(openChatDialog(QString,QString)));
  myServer->setForceHashCheck(dlg.checkMediaHash());
  if(!myServer->listen(QHostAddress::Any, dlg.port()))
  {
    ui->statusBar->showMessage(tr("Failed to listen on port %1").arg(dlg.port()));
    delete myServer;
    myServer = NULL;
    return;
  }

  ui->statusBar->showMessage(tr("Listening for connections..."), 3000);

  reconnectChatSlots();

  myNetworkType = Host;
}

void WindowMain::on_actionJoin_triggered()
{
  on_actionStopSynchronizing_triggered();

//  if(myCurrentFile.isEmpty())
//  {
//    QMessageBox::information(this, tr("Error"), tr("You have no media open for joining a server."));
//    return;
//  }

  DialogClientJoin dlg(this);
  int ret = dlg.exec();
  if(ret == QDialog::Rejected)
    return;

  myClient = new ClientTCP(myMPlayer, this);
  connect(myClient, SIGNAL(message(QString)), SLOT(showStatusBarMessage(QString)));
  connect(myClient, SIGNAL(chatMessage(QString,QString)), SLOT(openChatDialog(QString,QString)));
//  connect(myClient, SIGNAL(destroyed()),
  myClient->connectToHost(dlg.serverAddress(), dlg.port());
  ui->statusBar->showMessage(tr("Connecting..."));
  if(!myClient->waitForConnected())
  {
    ui->statusBar->showMessage(tr("Failed."));
    delete myClient;
    myClient = NULL;
    return;
  }
  ui->statusBar->showMessage(tr("Connected."));

  reconnectChatSlots();
//  myClient->checkMedia();

  myNetworkType = Client;
}

void WindowMain::showStatusBarMessage(const QString &msg)
{
  ui->statusBar->showMessage(msg, 3000);
}

void WindowMain::on_actionStopSynchronizing_triggered()
{
  if(myServer)
    delete myServer;
  if(myClient)
    delete myClient;
  myNetworkType = None;

  myServer = NULL;
  myClient = NULL;
}

void WindowMain::on_actionPlay_triggered()
{
  myMPlayer->startPlaying(myCurrentFile);
  if(myNetworkType == Host)
    myServer->synchronizeClients(myMPlayer->currentPlaybackSecond());
  else if(myNetworkType == Client)
    myClient->synchronize(myMPlayer->currentPlaybackSecond());
}

void WindowMain::on_actionPause_triggered()
{
  myMPlayer->pause();
  if(myNetworkType == Host)
    myServer->pauseClients();
  else if(myNetworkType == Client)
    myClient->pause();
}

void WindowMain::on_actionStop_triggered()
{
  myMPlayer->stop();
  if(myNetworkType == Host)
    myServer->stopClients();
  else if(myNetworkType == Client)
    myClient->stop();
}

void WindowMain::toggleFullScreen()
{
  myFullScreenFlag = !myFullScreenFlag;
  if(myFullScreenFlag)
  {
    ui->statusBar->hide();
    ui->mainToolBar->hide();
    ui->menuBar->hide();

    showFullScreen();
    myMPlayer->setFullScreen(true);
  }
  else
  {
    ui->statusBar->show();
    ui->mainToolBar->show();
    ui->menuBar->show();

    showNormal();
    myMPlayer->setFullScreen(false);
  }
}

// TODO: Make this controls bar float instead of resizing the video
void WindowMain::mouseMoveEvent(QMouseEvent *e)
{
  if(!myFullScreenFlag)
    return;

  if(e->pos().y() >= myScreen->height()-10)
  {
    ui->statusBar->show();
    ui->mainToolBar->show();
  }
  else if(e->pos().y() <= 10)
  {
    ui->menuBar->show();
    ui->statusBar->hide();
    ui->mainToolBar->hide();
  }
  else
  {
    ui->menuBar->hide();
    ui->statusBar->hide();
    ui->mainToolBar->hide();
  }
}

void WindowMain::on_actionChat_triggered()
{
  openChatDialog("", "");
}

void WindowMain::reconnectChatSlots()
{
  if(!myChatDialog)
    return;

  if(myClient)
  {
    myClient->disconnect(myChatDialog);
    myChatDialog->disconnect(myClient);
  }
  if(myServer)
  {
    myServer->disconnect(myChatDialog);
    myChatDialog->disconnect(myServer);
  }

  if(myNetworkType == Host)
  {
    connect(myChatDialog, SIGNAL(message(QString,QString)), myServer, SLOT(sendChatMessage(QString,QString)));
    connect(myServer, SIGNAL(chatMessage(QString,QString)), myChatDialog, SLOT(addMessage(QString,QString)));
  }
  else if(myNetworkType == Client)
  {
    connect(myChatDialog, SIGNAL(message(QString,QString)), myClient, SLOT(sendChatMessage(QString,QString)));
    connect(myClient, SIGNAL(chatMessage(QString,QString)), myChatDialog, SLOT(addMessage(QString,QString)));
  }
}

void WindowMain::openChatDialog(const QString &nick, const QString &msg)
{
  if(myChatDialog)
  {
    myChatDialog->show();
    myChatDialog->raise();
    myChatDialog->activateWindow();
    return;
  }

  myChatDialog = new DialogChat(this);
  connect(myChatDialog, SIGNAL(finished(int)), SLOT(closeChatDialog()));
  if(!nick.isEmpty() || !msg.isEmpty())
    myChatDialog->addMessage(nick, msg);

  if(myNetworkType == Host)
  {
    connect(myChatDialog, SIGNAL(message(QString,QString)), myServer, SLOT(sendChatMessage(QString,QString)));
    connect(myServer, SIGNAL(chatMessage(QString,QString)), myChatDialog, SLOT(addMessage(QString,QString)));
  }
  else if(myNetworkType == Client)
  {
    connect(myChatDialog, SIGNAL(message(QString,QString)), myClient, SLOT(sendChatMessage(QString,QString)));
    connect(myClient, SIGNAL(chatMessage(QString,QString)), myChatDialog, SLOT(addMessage(QString,QString)));
  }
  myChatDialog->show();
  myChatDialog->raise();
  myChatDialog->activateWindow();
}

void WindowMain::closeChatDialog()
{
//  myChatDialog = NULL;
}
