/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef LABELTIME_H
#define LABELTIME_H

#include <QLabel>

class ProcessMPlayer;

// TODO: Add reverse countdown of time

/**
 * @brief Converts from seconds and shows time in 00:00:00 / 00:00:00 format
 *
 */
class LabelTime : public QLabel
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit LabelTime(QWidget *parent = 0);

public slots:
  /**
   * @brief Sets the current time in the media
   *
   * @param secs
   */
  void setTime(int secs);
  /**
   * @brief Sets the duration of the media
   *
   * @param secs
   */
  void setDuration(int secs);

private:
  int myCurrentTime; /**< The current time of the media */
  int myDuration; /**< The duration of the media */

  /**
   * @brief Updates de displayed text
   *
   */
  void updateTime();
};

#endif // LABELTIME_H
