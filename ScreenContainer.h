/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef SCREENCONTAINER_H
#define SCREENCONTAINER_H

#include "Screen.h"

/**
 * @brief This container resizes the inner screen to the aspect ratio
 *
 */
class ScreenContainer : public Screen
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit ScreenContainer(QWidget *parent = 0);
  /**
   * @brief Returns the window ID of the inner screen
   *
   * @return WId
   */
  WId      windowId() const;

signals:
  void doubleClicked();

public slots:
  /**
   * @brief Sets the aspect ratio of the media
   *
   * @param ratio
   */
  void setAspectRatio(double ratio);

protected:
  /**
   * @brief Called on resize to correct the inner screen dimensions
   *
   * @param e
   */
  void resizeEvent(QResizeEvent *e);

  void mouseDoubleClickEvent(QMouseEvent *e);

  bool eventFilter(QObject *, QEvent *e);

  void timerEvent(QTimerEvent *e);

private:
  Screen* myScreen; /**< The inner screen */
  int myOffsetX; /**< Inner screen offset X */
  int myOffsetY; /**< Inner screen offset Y */
  int myOriginalX; /**< Inner screen offset X */
  int myOriginalY; /**< Inner screen offset Y */
  int myOriginalWidth; /**< Inner screen width */
  int myOriginalHeight; /**< Inner screen height */
  double myAspectRatio; /**< Media aspect ratio */
  int myHideCursorTimerId; /**< Timer for hiding the cursor pointer */

private slots:
  /**
   * @brief Called mostly from resizeEvent to correct the aspect of the media
   *
   */
  void updateScreen();
};

#endif // SCREENCONTAINER_H
