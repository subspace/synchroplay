/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "DialogChat.h"
#include "ui_DialogChat.h"
#include <QStringListModel>
#include <QTime>
#include <QCloseEvent>

DialogChat::DialogChat(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::DialogChat),
  myModel(new QStringListModel())
{
  ui->setupUi(this);

//  connect(this, SIGNAL(accepted()), SLOT(deleteLater()));
//  connect(this, SIGNAL(rejected()), SLOT(deleteLater()));
//  connect(this, SIGNAL(finished(int)), SLOT(deleteLater()));

  connect(ui->editMessage, SIGNAL(returnPressed()), SLOT(sendMessage()));
  ui->viewMessages->setModel(myModel);

  ui->editNick->setText(QString(tr("DeRp%1")).arg(qrand() & 0x7fffffff));
}

void DialogChat::sendMessage()
{
  QString nick(ui->editNick->text()), msg(ui->editMessage->text());
  emit message(nick, msg);

  ui->editMessage->clear();
}

void DialogChat::addMessage(const QString &nick, const QString &message)
{
  QStringList msgs = myModel->stringList();

  msgs.append("[" + QTime::currentTime().toString() + "] <" + nick + "> " + message);
  myModel->setStringList(msgs);
  ui->viewMessages->scrollToBottom();
}

DialogChat::~DialogChat()
{
  delete ui;
  delete myModel;
}
