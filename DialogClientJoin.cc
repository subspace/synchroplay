/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "DialogClientJoin.h"
#include "ui_DialogClientJoin.h"

DialogClientJoin::DialogClientJoin(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::DialogClientJoin)
{
  ui->setupUi(this);
  ui->editPort->setValidator(new QIntValidator(1, 65535, this));
  ui->editPort->setText("22777");
}

DialogClientJoin::~DialogClientJoin()
{
  delete ui;
}

QString DialogClientJoin::serverAddress() const
{
  return ui->editServer->text();
}

quint16 DialogClientJoin::port() const
{
  return ui->editPort->text().toUShort();
}
