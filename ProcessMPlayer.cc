/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "ProcessMPlayer.h"
#include "Common.h"
#include <QDebug>

ProcessMPlayer::ProcessMPlayer(QObject *parent) :
  QProcess(parent),
  myWindowID(0),
  myState(Stopped),
  myLastPlaybackSecond(0),
  myLoadedFlag(false),
  myFullScreenFlag(false),
  myPausingFlag(false)
{
  setProcessChannelMode(QProcess::MergedChannels);
  connect(this, SIGNAL(readyReadStandardOutput()), SLOT(readAndParse()));
//  connect(this, SIGNAL(readyRead()), SLOT(readAndParse()));
}

bool ProcessMPlayer::startPlaying(const QString &fileName, int secs)
{
  QString mediaFile = fileName.isEmpty() ? myCurrentFile : fileName;

  if(myState == Paused && (myCurrentFile == mediaFile) && state() == QProcess::Running)
  {
    pause();
    return true;
  }

  if(state() == QProcess::Running)
    stop();

  if(mediaFile.isEmpty())
    return false;

  if(!myWindowID)
  {
    qDebug("No window id set.");
    return false;
  }

  // Generate file hash here
  if(mediaFile != myCurrentFile)
    myCurrentFileHash.clear();

  start("mplayer", QStringList()
        << "-wid" << QString::number((qint64)myWindowID)
      #if defined(Q_OS_LINUX)
        << "-vo" << "xv"
      #elif defined(Q_OS_WIN)
        << "-vo" << "gl"
      #endif
        << "-double"
        << "-slices"
        << "-slave"
        << "-ss" << QString::number(secs)
        << "-identify"
        << mediaFile);

  bool r = waitForStarted();
  if(!r)
  {
    myState = Stopped;
    emit playing(false);
  }
  myCurrentFile = mediaFile;

  return r;
}

void ProcessMPlayer::setVolume(int volume)
{
  if(volume < 0)
    volume = 0;
  else if(volume > 100)
    volume = 100;
  sendCommand(QString("volume %1 1\n").arg(volume));
}

void ProcessMPlayer::setWindowID(WId windowID)
{
  myWindowID = windowID;
}

void ProcessMPlayer::stop()
{
  if(myState == Stopped)
    return;

  sendCommand("quit\n");
  if(!waitForFinished())
    kill();

  myState = Stopped;
  myLoadedFlag = false;

  emit playing(false);

  myLastPlaybackSecond = 0;
}

void ProcessMPlayer::pause()
{
  if(myPausingFlag)
    return;
  sendCommand("pause\n");
  myPausingFlag = true;
}

void ProcessMPlayer::seek(int secs, int mode)
{
  if(state() == QProcess::Running)
    sendCommand(QString("seek %1 %2\n").arg(secs).arg(mode));
}

void ProcessMPlayer::sendCommand(const QString &command)
{
  if(state() != QProcess::Running)
    return;
  write(command.toLatin1());
}

static QRegExp rxWindowResolution("^VO: \\[(.*)\\] (\\d+)x(\\d+) => (\\d+)x(\\d+)");
static QRegExp rxPlayback("^A:\\s*([0-9,.]+)\\s+V:\\s*([0-9,.]+)");
static QRegExp rxLength("^ID_LENGTH=([0-9,.]+)");
static QRegExp rxPause("^ID_PAUSED");

void ProcessMPlayer::readAndParse()
{
  QStringList lines = QString(readAll()).split('\n');

  foreach(QString line, lines)
  {
    line = line.trimmed();
//    qDebug() << line;
    if(rxPlayback.indexIn(line) != -1)
    {
      int sec = rxPlayback.cap(2).toDouble();
      if(sec != myLastPlaybackSecond)
      {
        myLastPlaybackSecond = sec;
        emit playbackSecond(sec);
      }

      if(myState == Paused || myState == Stopped)
      {
        myState = Playing;
        myPausingFlag = false;
        emit playing(true);
      }

      emit playback(sec);

      if(!myLoadedFlag)
      {
        emit loaded();
        myLoadedFlag = true;
      }
      continue;
    }

    if(rxPause.indexIn(line) != -1)
    {
      myState = Paused;
      myPausingFlag = false;
      emit playing(false);
      continue;
    }

    if(rxLength.indexIn(line) != -1)
    {
      emit playbackLength(rxLength.cap(1).toDouble());
      continue;
    }

    if(rxWindowResolution.indexIn(line) != -1)
    {
      int width = rxWindowResolution.cap(4).toInt();
      int height = rxWindowResolution.cap(5).toInt();

      emit windowResolution(width, height);
      emit aspectRatio(width/(float)height);

      continue;
    }
  }
}

int ProcessMPlayer::currentPlaybackSecond() const
{
  return myLastPlaybackSecond;
}

const QString& ProcessMPlayer::currentlyPlayingMedia() const
{
  return myCurrentFile;
}

bool ProcessMPlayer::isPlaying() const
{
  return (myState == Playing);
}

bool ProcessMPlayer::isStopped() const
{
  return (myState == Stopped);
}

bool ProcessMPlayer::isPaused() const
{
  return (myState == Paused);
}

void ProcessMPlayer::setFullScreen(bool fs)
{
  sendCommand(QString("vo_fullscreen %1\n").arg(fs));
}

const QByteArray& ProcessMPlayer::currentFileHash()
{
  if(myCurrentFileHash.isEmpty())
  {
    emit message(tr("Hashing media file..."));
    myCurrentFileHash = Common::MD5Hash(myCurrentFile);
  }
  return myCurrentFileHash;
}
