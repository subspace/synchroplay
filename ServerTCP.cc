/*
GNU General Public License version 3 notice

Copyright (C) 2013 Mihawk. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "ServerTCP.h"
#include "ServerTCPConnection.h"
#include "ProcessMPlayer.h"

ServerTCP::ServerTCP(ProcessMPlayer *mplayer, QObject *parent) :
  QTcpServer(parent),
  myMPlayer(mplayer),
  myForceHashCheckFlag(false)
{
  startTimer(5000);
}

void ServerTCP::incomingConnection(int handle)
{
  ServerTCPConnection* c = new ServerTCPConnection(myMPlayer, this);
  c->setSocketDescriptor(handle);
  myConnections.push_back(c);
  synchronizeClients(myMPlayer->currentPlaybackSecond());
  connect(c, SIGNAL(disconnected()), SLOT(clientDisconnected()));

  emit message(QString(tr("Client %1 connected.")).arg(c->peerAddress().toString()));

}

void ServerTCP::clientDisconnected()
{
  ServerTCPConnection *c = qobject_cast<ServerTCPConnection *>(sender());
  myConnections.removeAll(c);
  c->deleteLater();

  emit message(QString(tr("Client %1 disconnected.")).arg(c->peerAddress().toString()));
}

void ServerTCP::synchronizeClients(int second)
{
  foreach(ServerTCPConnection* c, myConnections)
  {
    c->synchronize(second);
  }

  if(!myMPlayer->isPlaying())
    myMPlayer->startPlaying();
  myMPlayer->seek(second);

  emit message(tr("Synchronizing..."));
}

void ServerTCP::pauseClients()
{
  foreach(ServerTCPConnection* c, myConnections)
  {
    c->pause();
  }
  emit message(tr("Pausing clients..."));
}

void ServerTCP::stopClients()
{
  foreach(ServerTCPConnection* c, myConnections)
  {
    c->stop();
  }
  emit message(tr("Stopping clients..."));
}

void ServerTCP::sendChatMessage(const QString &nick, const QString &message)
{
  foreach(ServerTCPConnection* c, myConnections)
  {
    c->sendChatMessage(nick, message);
  }
  emit chatMessage(nick, message); // Send the message to myself
}

void ServerTCP::checkMedia()
{
  foreach(ServerTCPConnection* c, myConnections)
  {
    c->checkMedia();
  }
}

void ServerTCP::setForceHashCheck(bool f)
{
  myForceHashCheckFlag = f;
}

bool ServerTCP::forceHashCheck() const
{
  return myForceHashCheckFlag;
}

void ServerTCP::timerEvent(QTimerEvent *)
{
  if(!myForceHashCheckFlag)
    return;
  checkMedia();
}
